#pragma once

#include <sstream>
#include <iostream>
#include <string>
#include "phone_number.h"

using namespace std;


	PhoneNumber::PhoneNumber(const string &international_number) {
		istringstream in(international_number);
		country_code_ = city_code_ = local_number_ = "";
		int n = 100;
		char* str = new char[n];
		str[0] = '\0';
		if (in.peek() != '+') throw invalid_argument("Wrong phone number: " + international_number);
		in.ignore(1);
		in.getline(str, sizeof(str), '-');
		if (!in || str[0] == '\0') throw invalid_argument("Wrong phone number: " + international_number);
		country_code_ = string(str);
		str[0] = '\0';
		in.getline(str, sizeof(str), '-');
		if (!in || str[0] == '\0') throw invalid_argument("Wrong phone number: " + international_number);
		city_code_ = string(str);
		in >> local_number_;
		if (local_number_ == "") throw invalid_argument("Wrong phone number: " + international_number);
	}

	string PhoneNumber::GetCountryCode() const {
		return country_code_;
	}
	string PhoneNumber::GetCityCode() const {
		return city_code_;
	}
	string PhoneNumber::GetLocalNumber() const {
		return local_number_;
	}
	string PhoneNumber::GetInternationalNumber() const {
		return "+" + country_code_ + "-" + city_code_ + "-" + local_number_;
	}
//one more comment
int main() {
	try {
		PhoneNumber r("+7-495-111-22-33");
		cout << r.GetInternationalNumber();
	}
	catch (...) {
		cout << "Oops";
	}
	return 0;

}
